FROM nginxinc/nginx-unprivileged

COPY  public/ /usr/share/nginx/html/
COPY  start.sh /usr/share/nginx/html/hostname.html

COPY start.sh /start.sh

ENTRYPOINT ["/start.sh"]

