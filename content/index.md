---
title: "Štěňata"
date: 2021-10-09T12:56:29+02:00
---


<div id="live"></div>

# O Čintě a štěňatech

![Činty táta](/images/cinty_tata.jpg)

A takhle to všechno začalo. Když taťka Činty skočil k její mamce do chalupy oknem, bylo jasné, že je zaděláno na štěňátka. Mně, jako nastávající majitelce jednoho z sedmi štěňátek, se opravdu jen těžko věřilo, že by si border koliák vybral právě hárající border kolii a namířil si to přímo k ní do ložnice. Činty maminka (Connie, Velký Dřevíč u Náchoda) je typická černobílá border kolie, její rodiče byli papíroví, ona už papíry nemá. Tatínek (Sovík, Velký Dřevíč u Náchoda)  je výrazně mohutnější postavy a zbarvení blue merle a jeho rodokmen je k vidění [zde](http://db.bordercollie.ru/details.php?id=100615&fbclid=IwAR0nUY3PvCXXuPMzRW72AlPVgwuN3T7Occ07x2rHDADCPjBaUVMFINlEr5g).


![Činty máma](/images/cinty_mama.jpg)
Společně se jim narodilo 7 štěňátek, 4 blue merle a 3 černobílá.


![Činta s Boonie](/images/cinta_boonie.jpg)
Činta podědila alespoň část blue merle genů v podobě černých flíčků na bílých částech na tlapkách a bříšku. Netradičně má také celou hlavičku černou. Bílý flíček na ocase ale rozhodně nechybí.


![Činta s ovcí](/images/cinta_ovce.jpg)
Činta má typickou bordeří povahu, tedy spíše agilitního zaměření. Míček nebo frisbee je pro ní svátost. Zato ovečky vůbec nevyhledává. Ovečky vyhledávají spíš jí a ona před nimi utíká nebo je jen zdálky pozoruje.



![Malá Činta](/images/cinta_mala.jpg)
Už jako malá byla neuvěřitelně poslušná. Paní na cvičáku jí dokonce říkala stará mladá, jelikož až podezřele poslouchala a bez odmlouvání se učila všechny další povely. Ani během období puberty nebylo okamžiku, kdy by se nenechala odvolat. Jako štěně samozřejmě ztropila pár lotrovin v podobě krácení síťových kabelů, kabelů klávesnic a myší a ničení páskových sandálů. Z toho všeho ale během prvního roku vyrostla.



![Činta v Norsku](/images/cinta_kolo.jpg)
Většinou si vůbec nevšímá ostatních pejsků ani zvířat jakéhokoliv druhu, raději se jim vyhne. Proto také bez problému prakticky pořád chodíme na volno a nemusíme se bát žádného konfliktu. Nemyslím si, že border kolie musí mít k dispozici velkou zahradu nebo dokonce celou louku. Samozřejmě, je to lepší. Potřebuje ale hlavně páníčka, který se jí věnuje. My s Čintou podnikáme prakticky vše a nijak nás neomezuje.



![Činta kontroluje Radku jestli spadne](/images/cinta_radka.jpg)
Pěší vycházky jsou samozřejmostí, těžší terén na kole s pomalejším tempem je pro ni ideální, výlet na běžkách v zimě si taky moc užívá.

![Činta valí borůvkama](/images/cinta_norsko.jpg)
Procestovala s námi Norsko během pěti týdnů dodávkou, kolikrát na trailu zapadla do bahna až po bříško, zvládla i známý strmý pěší stoupák na [Bessegen Ridge](https://www.visitnorway.com/places-to-go/eastern-norway/the-jotunheimen-mountains/hiking-the-besseggen-ridge/).


![Činta skáče do vln](/images/cinta_plaz.jpg)
Činta miluje vodu. Dokáže si dělat kolečka v rybníce a štěkat si do vody i dobrou hodinu. Ovládá i skok do vody z poměrně velké výšky, užívá si vln v moři.

![Činta v tunelu](/images/cinta_tunel.jpg)
A velmi často aplikuje svůj oblíbený povel “tunel” z cvičáku a když je plný vody, je to ještě výrazně lepší.

![Činta se vyhlíží](/images/cinta_vyhled.jpg)
Border kolie obecně nejsou psem jednoho pána. Mají své lidské stádečko, jakmile ji ale uplatíte míčkem, odejde s kýmkoliv. To má i výhodu v tom, že ji můžete nechat pohlídat komukoliv a ona ho bude poslouchat. Občas bývají border kolie využívány i ke canysterapii. Činta to ale rozhodně nemá v genech. Při občasném mazlení z mé strany útrpně drží a v hlavě se jí honí jen to, že když mám čas na takovéto něžnosti, proč radši nezkusit něco zábavnějšího jako házení míčkem. [^1] Každopádně je to ale nejlepší psí parťák, jakého si člověk může přát.

![Flíček](/images/flicek.jpg)
A pět let poté, co Činta přišla na svět, si rande domluvila ona s fešákem z nedaleké vesnice od naší chaty. Na procházce si ho čumáčkem na čumák přes plot zkontrolovala a dala mu jasně najevo, že to by teda šlo. Krásně zbarvenému hnědobílému red border koliákovi. Po pěti letech hárání, které se klasicky neprojevovalo ani sebemenším zájmem o opačné psí pohlaví. Když tedy opomenu incident s jakýmsi Knödllem na procházce v Riegrových sadech, jehož jméno přesně vystihovalo jeho tělesnou stavbu a původ. Ale to byla ještě štěňátko a neměla z toho rozum. Nabyla jsem tedy dojmu, že Čintička o pejsky prostě nejeví zájem a středobodem jejího světa i v tomto období zůstává tenisák.

![Odpočinek po akci](/images/sex.jpg)
A tak, když se krasavec Flíček z vedlejší vesnice připlížil lesem (přeskočil plot, soused ho prý viděl, jak se tiše plíží křovím), bylo překvapením, že se Čintička ničemu nebránila a nechala volnost svým pudům. Flíček bydlí se svým tátou a páníčky v rodinném domě s velkou zahradou ve vesničce [Slatina](https://en.mapy.cz/s/nofacojave) kousek od Strakonic. Hnědobílou barvu podědil po svém dědovi, oba jeho rodiče byli černobílí. Je poměrně mohutný a dle majitelů dominantní, ale i tak je to prý páníčkův mazel.


![Činta kojí](/images/cinta_koji.jpg)
A pak uběhly dva měsíce a 3. října se narodilo krásných 7 štěňátek - 3 fenky a 4 psíci (jak nám až při prohlídce veterináře bylo osvětleno :) ). A tak mají pejsci občas holčičí pracovní jména a fenky zase klučičí. Nevadí, je jim to stejně jedno. 


![Pokus o kojení](/images/kojeni.jpg)
Činta byla po porodu trošku překvapená a první den to vypadalo spíš na matku distančního ražení a tak jsme zkusili divočáky přikrmit mlékem.

Ale druhý den už se z ní stala správná mamka, která přiběhne hned, jak jeden malý zakňourá. Později budou krmena štěněcími granulemi Arden Grange a všelijakými dobrůtkami.



## Info pro nové majitele

### Odčervení
Štěňátka byla odčervena přípravkem Drontal Junior ve věku 3, 5, a 9 týdnů, další odčervení by mělo proběhnout ve 13. týdnu a dále ve věku **4, 5, a 6 měsíců**.

### Očkování
Štěňata byla poprvé očkována 21.11. (parvovirus a psinka). Druhé očkování proběhlo 11.12. Třetí očkování by mělo být 3-4 týdny poté, tedy v týdnu od 3.1.2022 (možno přidat i očkování na psí kašel). 

### Krmení
Štěňata jsou od 4. týdne postupně přikrmována granulemi v kombinaci s psím mlékem Lactol. Od 6. týdne postupně přešla na tuhou stravu (mix granulí Arden Grange Weaning and Puppy bez obilovin a granulí Farmina NaD Dog Puppy Low Grain s nízkým obsahem obilovin - aby si štěňátka zvykla i částečný přísun lepku + občas ochucené štěněcími masovými konzervami). Granule Arden Grange Weaning and Puppy v 8. týdnu nahradíme granulemi Arden Grange Puppy Junior (určené do 9 měsíců věku) v kombinaci s Farmina NaD Dog Puppy Low Grain. Štěňátko by v novém domově mělo dostávat už jen granule Arden Grange Puppy Junior (pokud budete chtít změnit výrobce granulí, je ideální nejdříve mixovat s Arden Grange). 

Štěňátka dostávají 4 porce denně, celkem cca 300g dávky (ráno po prvním venčení (cca 07:30), cca kolem 11-12. hod, 15. hod, 19.hod). Ideální je vzít je ven hned po probuzení vyčůrat, pak dát najíst, poté začnou řádit, nechat případně znovu vyčůrat, pak usnou :). Venku vydrží i v těchto teplotách (kolem 5°C přes den) v pohodě klidně hodinku. Pokud chcete štěně uklidnit, dejte mu kus dřívka na kousání, to ho velmi zabaví. Většina z nich už chápe povel NE, především když okusují doma nábytek. 


### Vybavení, se kterým jsme spokojeni
* [Trimovací kartáč](https://www.spokojenypes.cz/hrablo-s-rotacnimy-zuby-antiskluz-rukojet-10x15cm-zuby-2cm/?gclid=Cj0KCQjw8p2MBhCiARIsADDUFVEntIA5pEFKuGR2X4jTXJJJbpc17aAPeqh4SESwnxVVDu9L1QWKKyQaAop9EALw_wcB)
* [Postroj na běhání/kolo/běžky/pěší výlety](https://www.zerodc.cz/cz/produkty/postroje/tazne-postroje/tazny-postroj-faster.html)
* [Pružné vodítko na běhání/kolo/běžky/pěší výlety](https://www.behejsepsem.cz/shop/pro-psy/voditka/bungee-leash-double/)
* [Sedák na běhání/běžky/pěší výlety](https://www.behejsepsem.cz/shop/pro-lidi/sedaky/bezecky-opasek/)
* [Zimní botičky do sněhu na běhání/běžky](https://www.behejsepsem.cz/shop/pro-psy/pece/zimni-boticka-modra-4ks/) (pro teploty okolo 0°C, kdy se pejskům dělají namrzlé koule na tlapkách)
* [Cestovní vak na granule](https://www.sportisimo.cz/jr-gear/lodni-vak-30l-light-weight/97757/) (skladný a smradu nepropustný)

## Odběr
Štěňátka budou po pečlivé socializaci dospělými, dětí všech věků, ostatními pejsky, a po prezentaci všech cyklo dílů a nářadí, k předání v 11. týdnu věku, tedy v sobotu 18.12. v odpoledních hodinách (ideální bude, když půjdou štěňata do nového domova ve stejný den). Případně po domluvě možné i v neděli 19.12. 


---
## 4 týdny
![](/images/w2/spolecna1.jpg)
![](/images/w2/spolecna2.jpg)
![](/images/w2/spolecna3.jpg)
![](/images/w2/spolecna4.jpg)
![](/images/w2/spolecna5.jpg)
![](/images/w2/spolecna6.jpg)
![](/images/w2/spolecna7.jpg)

---
## 7 týdnů
![](/images/w7/spolecna1.jpg)
![](/images/w7/spolecna2.jpg)
![](/images/w7/spolecna3.jpg)
![](/images/w7/spolecna4.jpg)
![](/images/w7/spolecna5.jpg)
![](/images/w7/spolecna6.jpg)
![](/images/w7/spolecna7.jpg)
![](/images/w7/spolecna8.jpg)

---
## 8 týdnů
![](/images/w8/spolecna1.jpg)
![](/images/w8/spolecna2.jpg)
![](/images/w8/spolecna3.jpg)
![](/images/w8/spolecna4.jpg)
![](/images/w8/spolecna5.jpg)
![](/images/w8/spolecna6.jpg)
![](/images/w8/spolecna7.jpg)
![](/images/w8/spolecna8.jpg)
![](/images/w8/spolecna9.jpg)
![](/images/w8/spolecna10.jpg)


---
## Malá Činta (Joujou)
![Malá Činta](/images/w1/cinta.jpg)
![Malá Činta](/images/w2/cinta.jpg)
![Malá Činta](/images/w8/cinta.jpg)
Černobílá fenka dostala pracovní název Malá Činta. Postupně se jí na bílých částech objevily černé flíčky po mamince. Malá Činta nakonec zůstane s mámou a bude jí dělat parťáka na lumpárny všeho druhu.


---
## Flíček (Oggie)
![Flíček](/images/w1/flicek.jpg)
![Flíček](/images/w2/flicek.jpg)
![Flíček](/images/w8/flicek.jpg)
Další dvě fenky jsou šedo-bílé. Flíček má flek na hlavičce a narodila se venku pod borovicí, jelikož přísun čerstvého vzduchu dělal Čintě vyloženě dobře a mohla si i občas poměrně kvalitně pohrabat. Postupně se jí na bílých částech objevili šedé flíčky. Páníčci jí vybrali jméno Oggie. Bude bydlet v Bohnicích v domečku se zahrádkou a její noví páníčci s ní budou ve volném čase cestovat s dodávkou. 


---
## Eliáš (Poppy von Schatz)
![Eliáš](/images/w1/elias.jpg)
![Eliáš](/images/w2/elias.jpg)
![Eliáš](/images/w8/elias.jpg)
Druhá šedobílá fenka dostala pracovní jméno Eliáš, má flíček za krkem, a narodila se na terase u břečťanu. Na bílých částech má také šedé flíčky. Dostala jméno Poppy. Poppy si bude se svým páníčkem a paničkou užívat krásy Krušných Hor, přes den se bude věnovat prodeji kol v jednom bikovém obchůdku nedaleko Božího Daru a o víkendech se bude prohánět za kolem místními lesy. 


---
## Činťák 
![Činťák](/images/w1/cintak.jpg)
![Činťák](/images/w2/cintak.jpg)
![Činťák](/images/w8/cintak.jpg)
Černobílý pejsek byl prvním přírůstkem a říkáme mu malý Činťák. Toto jméno mu také zůstane. Má jen malý flíček na hlavě a za krkem, tak je to dokonalá kopie maminky. Činťák se bude v 11. týdnu stěhovat do Trutnova do rodiny bikerů a jeho novému páníčkovi bude dělat společnost na místních trailech.


---
## Véčko (Atray)
![Véčko](/images/w1/vecko.jpg)
![Véčko](/images/w2/vecko.jpg)
![Véčko](/images/w8/vecko.jpg)
Pak tu máme šedobílého Véčka, který má na hlavičce bílou část zakončenou písmenem V. Bílé části zůstali sněhobílé. Bude se jmenovat Atray. Atray bude se bude starat o velké hospodářství a pást ovečky v Královehradeckém kraji. Ve volných chvílích bude mazlen dvěma malými holčičkami. 


---
## Zrzek (Fox)
![Zrzek](/images/w1/zrzek.jpg)
![Zrzek](/images/w2/zrzek.jpg)
![Zrzek](/images/w8/zrzek.jpg)
Velmi speciální je hnědobílý Zrzoun (jelikož při porodu v noci pod infralampou se vám poměrně hodně zkreslují barvy), který bude nejspíš po tatínkovi. Tmavě hnědá barva mu zůstala, bílé části jsou bez flíčků. 

Zrzounek Fridrich II. nakonec dostal jméno Fox. Bude z něj Vinohradský švihák se vším všudy, společnost mu budou dělat dva kočičáci.

---
## Pašík
![Pašík po narození](/images/pasik.jpg)
![Pašík](/images/w1/pasik.jpg)
![Pašík](/images/w2/pasik.jpg)
![Pašík](/images/w8/pasik.jpg)
Poslední je náš oblíbenec, Pašík. Toto jméno si zasloužil pro své proužky na šedém tělíčku a tak byl od malého prasátka opravdu k nepoznání. Bohužel se zdá, že s časem proužky ztrácí. Na bílých částech se mu ale objevily šedé flíčky. Pašík (jméno mu pro úspěch zůstane :) ) se bude před Vánocemi stěhovat na jih, nedaleko Písku. Čekat tam na něj bude nový parťák, border koliák.



<script>
if (window.location.host != "pejsci.6shore.net") {
/*
	var content = `
<h1>Živý přenos z pelechu</h1>

<p>
<iframe width="100%" height="430" src="https://www.youtube.com/embed/IyZs8aBtpjw" title="Video z pelechu" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>
`;
*/

	var content = `
<h1>Aktuální stav pelechu</h1>
<p class="markdown-image">
<a href="/images/cam-latest.jpg"><img src="/images/cam-latest.jpg" /></a>
</p>
	`;

	
	document.getElementById("live").innerHTML = content;
}
</script>

