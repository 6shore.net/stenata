#!/bin/bash -xee

env
id

# write hostname
ls -lah /usr/share/nginx/html
export HOSTNAME="${VES_IO_SITETYPE} ${VES_IO_COUNTRY} ${VES_IO_REGION} ${VES_IO_SITENAME}"
echo $HOSTNAME | tee /usr/share/nginx/html/hostname.html

cat /usr/share/nginx/html/hostname.html

sed -e "s/<!-- HOSTNAME -->/${HOSTNAME}/g" /usr/share/nginx/html/index.html > /tmp/index.html
cat /tmp/index.html > /usr/share/nginx/html/index.html

# start nginx
exec nginx -g 'daemon off;'
